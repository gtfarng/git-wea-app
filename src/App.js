import React, { Component } from 'react';
import Github from './Component/Github'
import Weather from './Component/Weather'
import './App.css'
import logo from './logo.svg';

export default class App extends Component {
  render() {
    return (
      <div>

        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <br /><h1 class="f-head">My-ReactApp</h1><br />
        </div>

        <div className='all'>
          <Github />
        </div>

        <div className='all'>
          <Weather />
        </div>

      </div>
    );
  }
}


