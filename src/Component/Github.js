import React, { Component } from 'react'
import axios from 'axios'
import '../App.css'

export default class Github extends Component {
    state = {
        user: 'gtfarng',
        data: '',
        isLoading: true
    }

    componentDidMount = () => {
        this.fetchUser(this.state.user)
    }

    fetchUser = (USER) => {
        axios.get(`https://api.github.com/users/${USER}`).then(response => {
            this.setState({ data: response.data })
            console.log(response.data)
        }).finally(() => {
            this.setState({ isLoading: false })
        })
    }

    handleChagne = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const { data, isLoading } = this.state
        return (
            <div><br />
                {isLoading === true && (
                    <h1>Loading</h1>
                )}
                {isLoading === false && (
                    <div align='center'>
                        <div className="container">

                            <div class="card github">
                                <div class="card-body">
                                    <input name="user" class="form-group mx-sm-3 mb-2" placeholder="Username..." onChange={this.handleChagne} />
                                    <button class="btn btn-primary" onClick={() => this.fetchUser(this.state.user)}>Search</button><br />
                                    <h1>{this.props.user}</h1>
                                    <p class='f'><strong>Username :</strong> {data.login} <br /></p>
                                    <p class='f'><strong>Name :</strong> {data.name} <br /></p>
                                    <p class='f'><strong>URL :</strong> {data.blog} <br /></p>
                                    <img src={data.avatar_url} alt="avatar" width="150px" />
                                </div>
                            </div>

                        </div>

                    </div>
                )}
            </div>
        )
    }
}
